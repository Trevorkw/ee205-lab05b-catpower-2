/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.h
/// @version 1.0
///
/// @author Trevor Chang <trevorkw@hawaii.edu>
/// @date 13 Feb 2022
/////////////////////////////////////////////////////////////////////////////
//

#pragma once

const double GASOLINEGALLONEQUIVALENTS_IN_A_JOULE = 1/(1.213e8);

const char GASOLINEGALLONEQUIVALENT = 'g';

extern double fromGasolineGallonEquivalentToJoule( double gasolinegallonequivalent );

extern double fromJouleToGasolineGallonEquivalent( double joule );

