///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @author Trevor Chang <@trevorkw@hawaii.edu>
/// @date   @13 Feb 2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include "ev.h"
#include "m.h"
#include "gge.h"
#include "f.h"
#include "cat.h"
//#define DEBUG


const char JOULE                    = 'j';

/*
const double ELECTRON_VOLTS_IN_A_JOULE = 6.24150974e18;
const double MEGATON_IN_A_JOULE = 1/(4.18e15);
const double GASOLINEGALLONEQUIVALENTS_IN_A_JOULE = 1/(1.213e8);
const double FOE_IN_A_JOULE = 1/(1e24);
const double CATPOWER_IN_A_JOULE = 0;

const double MEGATON_IN_AN_ELECTRON_VOLT = 1/(2.611446262e34);
const double GASOLINEGALLONEQUIVALENTS_IN_AN_ELECTRON_VOLT = 1/(7.5709346016053e26);
const double FOE_IN_AN_ELECTRON_VOLT = 1/(6.2414959617521e62);
const double CATPOWER_IN_AN_ELECTRON_VOLT = 0;

const double GASOLINEGALLONEQUIVALENT_IN_A_MEGATON = 3.1754705e7;
const double FOE_IN_A_MEGATON = 1/(2.3900573613767e28);
const double CATPOWER_IN_A_MEGATON = 0;

const double FOE_IN_A_GASOLINEGALLONEQUIVALENT = 1/(8.2440230832646e35);
const double CATPOWER_IN_A_GASOLINEGALLONEQUIVALENT = 0;

const double CATPOWER_IN_A_FOE = 0;


const char ELECTRON_VOLT            = 'e';
const char MEGATON                  = 'm';
const char GASOLINEGALLONEQUIVALENT = 'g';
const char FOE                      = 'f';
const char CATPOWER                 = 'c';


double fromElectronVoltsToJoule( double electronVolts ) {
   return electronVolts / ELECTRON_VOLTS_IN_A_JOULE ;
}


double fromJouleToElectronVolts( double joule ) {
   return joule * ELECTRON_VOLTS_IN_A_JOULE;
}

double fromMegatonToJoule( double megaton ) {
   return megaton / MEGATON_IN_A_JOULE;
}

double fromJouleToMegaton( double joule ) {
   return joule * MEGATON_IN_A_JOULE;
}

double fromGasolineGallonEquivalentToJoule( double gasolinegallonequivalent ) {
   return gasolinegallonequivalent / GASOLINEGALLONEQUIVALENTS_IN_A_JOULE;
}

double fromJouleToGasolineGallonEquivalent( double joule ) {
   return joule * GASOLINEGALLONEQUIVALENTS_IN_A_JOULE;
}

double fromFoeToJoule( double foe ) {
   return foe / FOE_IN_A_JOULE;
}

double fromJouleToFoe( double joule ) {
   return joule * FOE_IN_A_JOULE;
}

double fromCatPowertoJoule( double catPower ) {
        return 0.0;
}

double fromJouleToCatPower( double joule ) {
   return 0.0;
}
*/      //These are put in the .h/.cpp files






/*


double fromMegatonToElectronVolts( double megaton ) {
   return megaton / MEGATON_IN_AN_ELECTRON_VOLT;
}
double fromElectronVoltsToMegaton( double electronvolts ) {
   return electronvolts * MEGATON_IN_AN_ELECTRON_VOLT;
}

double fromGasolineGallonEquivalentToElectronVolts( double gasolinegallonequivalent ) {
   return gasolinegallonequivalent / GASOLINEGALLONEQUIVALENTS_IN_AN_ELECTRON_VOLT;
}

double fromElectronVoltsToGasolineGallonEquivalent( double electronvolt ) {
   return electronvolt * GASOLINEGALLONEQUIVALENTS_IN_AN_ELECTRON_VOLT;
}

double fromFoeToElectronVolts( double foe ) {
   return foe / FOE_IN_AN_ELECTRON_VOLT;
}

double fromElectronVoltsToFoe( double electronvolts ) {
   return electronvolts * FOE_IN_AN_ELECTRON_VOLT;
}

double fromElectronVoltsToCatPower( double electronvolts ) {
   return 0.0;
}

double fromCatPowertoElectronVolts(double catPower ) {
         return 0.0;
}

double fromGasolineGallonEquivalentToMegaton( double gasolinegallonequivalent ) {
   return gasolinegallonequivalent / GASOLINEGALLONEQUIVALENT_IN_A_MEGATON;
}

double fromMegatontoGasolineGallonEquivalent( double megaton ) {
   return megaton * GASOLINEGALLONEQUIVALENT_IN_A_MEGATON;
}

double fromFoeToMegaton( double foe ) {
   return foe / FOE_IN_A_MEGATON;
}

double fromMegatonToFoe( double megaton ) {
   return megaton * FOE_IN_A_MEGATON;
}

double fromMegatonToCatPower( double megaton ) {
   return 0.0;
}
double fromCatPowertoMegaton( double catPower) {
         return 0.0;
}

double fromFoeToGasolineGallonEquivalent( double foe ) {
   return foe / FOE_IN_A_GASOLINEGALLONEQUIVALENT;
}

double fromGasolineGallonEquivalentToFoe( double gasolinegallonequivalent ) {
   return gasolinegallonequivalent * FOE_IN_A_GASOLINEGALLONEQUIVALENT;
}

double fromGasolineGallonEquivalentToCatPower( double gasolinegallonequivalent ) {
   return 0.0;
}

double fromCatPowertoGasolineGallonEquivalent( double catPower ) {
         return 0.0;
}

double fromFoeToCatPower( double foe ) {
   return 0.0;
}

double fromCatPowertoFoe( double catPower ) {
   return 0.0;
}

*/                 //extra code to use if doing catpower differently




int usabilityPrint(int argc){
   if(argc != 4){
   printf( "Energy converter\n" );

   printf( "Usage:  catPower fromValue fromUnit toUnit\n" );
   printf( "   fromValue: A number that we want to convert\n" );
   printf( "   fromUnit:  The energy unit fromValue is in\n" );
   printf( "   toUnit:  The energy unit to convert to\n" );
   printf( "\n" );
   printf( "This program converts energy from one energy unit to another.\n" );
   printf( "The units it can convert are: \n" );
   printf( "   j = Joule\n" );
   printf( "   e = eV = electronVolt\n" );
   printf( "   m = MT = megaton of TNT\n" );
   printf( "   g = GGE = gasoline gallon equivalent\n" );
   printf( "   f = foe = the amount of energy produced by a supernova\n" );
   printf( "   c = catPower = like horsePower, but for cats\n" );
   printf( "\n" );
   printf( "To convert from one energy unit to another, enter a number \n" );
   printf( "it's unit and then a unit to convert it to.  For example, to\n" );
   printf( "convert 100 Joules to GGE, you'd enter:  $ catPower 100 j g\n" );
   printf( "\n" );
   return 1;
   }
   return 0;
}


int main( int argc, char* argv[] ) {
   double fromValue;
   char   fromUnit;
   char   toUnit;

   if(usabilityPrint(argc)== 1 ){
      return 0;
   }


   fromValue = atof( argv[1] );
   fromUnit = argv[2][0];         // Get the first character from the second argument
   toUnit = argv[3][0];           // Get the first character from the thrid argument
   
#ifdef DEBUG
   printf( "fromValue = [%lG]\n", fromValue ); //debugg Statment
   printf( "fromUnit = [%c]\n", fromUnit );
   printf( "toUnit = [%c]\n", toUnit );
#endif
   double commonValue;
   switch( fromUnit ) {
      case JOULE                    : commonValue = fromValue; // No conversion necessary
                                      break;
      case ELECTRON_VOLT            : commonValue = fromElectronVoltsToJoule( fromValue );
                                      break;
      case MEGATON                  : commonValue = fromMegatonToJoule( fromValue );
                                      break;
      case GASOLINEGALLONEQUIVALENT : commonValue = fromGasolineGallonEquivalentToJoule( fromValue );
                                      break;
      case FOE                      : commonValue = fromFoeToJoule( fromValue);
                                      break;
      case CATPOWER                 : commonValue = fromCatPowertoJoule(  );
                                      break;
      default                       : printf("Unknown fromUnit %c\n", fromUnit);
                                      break;
   }

   double toValue;
   switch( toUnit )  {
      case JOULE                    : toValue = commonValue;
                                      break;
      case ELECTRON_VOLT            : toValue = fromJouleToElectronVolts( commonValue );
                                      break;
      case MEGATON                  : toValue = fromJouleToMegaton( commonValue );
                                      break;
      case GASOLINEGALLONEQUIVALENT : toValue = fromJouleToGasolineGallonEquivalent( commonValue );
                                      break;
      case FOE                      : toValue = fromJouleToFoe( commonValue );
                                      break;
      case CATPOWER                 : toValue = fromJouleToCatPower(  );
                                      break;
      default                       : printf("Unknown toUnit %c\n", toUnit);
                                      break;

   }
#ifdef DEBUG
   printf( "commonValue = [%lG] joule\n\n", commonValue );
#endif
   printf( "%lG %c is %lG %c\n", fromValue, fromUnit, toValue, toUnit );
   return 0;
}
