/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file m.h
/// @version 1.0
///
/// @author Trevor Chang <trevorkw@hawaii.edu>
/// @date 13 Feb 2022
/////////////////////////////////////////////////////////////////////////////
//

#pragma once

const double MEGATON_IN_A_JOULE = 1/(4.18e15) ;
const char MEGATON = 'm';
extern double fromMegatonToJoule( double megaton ) ;
extern double fromJouleToMegaton( double joule ) ;

