/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file f.h
/// @version 1.0
///
/// @author Trevor Chang <trevorkw@hawaii.edu>
/// @date 13 Feb 2022
/////////////////////////////////////////////////////////////////////////////
//

#pragma once
const double FOE_IN_A_JOULE = 1/(1e24);
const char FOE = 'f';

extern double fromFoeToJoule( double foe );
extern double fromJouleToFoe( double joule );

