/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.cpp
/// @version 1.0
///
/// @author Trevor Chang <trevorkw@hawaii.edu>
/// @date 13 Feb 2022
/////////////////////////////////////////////////////////////////////////////
//
#include "gge.h"
double fromGasolineGallonEquivalentToJoule( double gasolinegallonequivalent ) {
   return gasolinegallonequivalent / GASOLINEGALLONEQUIVALENTS_IN_A_JOULE;
}
double fromJouleToGasolineGallonEquivalent( double joule ) {
   return joule * GASOLINEGALLONEQUIVALENTS_IN_A_JOULE;
}
