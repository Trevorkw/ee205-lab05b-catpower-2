###############################################################################
### University of Hawaii, College of Engineering
### @brief Lab 05b - catPower 2 - EE 205 - Spr 2022
###
### @file Makefile
### @version 1.0 - Initial version
###
### Build and test an energy unit conversion program
###
### @author Trevor Chang <trevorkw@hawaii.edu>
### @date  13 Feb 2022
###
### @see https://www.gnu.org/software/make/manual/make.html
###############################################################################
CC = g++
CFLAGS = -g -Wall -Wextra
TARGET = catPower
all: $(TARGET)
ev.o: ev.cpp ev.h
	$(CC) $(CFLAGS) -c ev.cpp


m.o: m.cpp m.h
	$(CC) $(CFLAGS) -c m.cpp
gge.o: gge.cpp gge.h
	$(CC) $(CFLAGS) -c gge.cpp
f.o: f.cpp f.h
	$(CC) $(CFLAGS) -c f.cpp
cat.o: cat.cpp cat.h
	$(CC) $(CFLAGS) -c cat.cpp


catPower.o: catPower.cpp ev.h m.h gge.h f.h cat.h
	$(CC) $(CFLAGS) -c catPower.cpp
catPower: catPower.o ev.o m.o gge.o f.o cat.o
	$(CC) $(CFLAGS) -o $(TARGET) catPower.o ev.o m.o gge.o f.o cat.o 
clean:
	rm -f $(TARGET) *.o

test: catPower
	@./catPower 3.14 j j | grep -q "3.14 j is 3.14 j"
	@./catPower 3.14 j e | grep -q "3.14 j is 1.95983E+19 e"
	@./catPower 3.14 j m | grep -q "3.14 j is 7.51196E-16 m"
	@./catPower 3.14 j g | grep -q "3.14 j is 2.58862E-08 g"
	@./catPower 3.14 j f | grep -q "3.14 j is 3.14E-24 f"  
	@./catPower 3.14 j c | grep -q "3.14 j is 0 c"  
	@./catPower 3.14 j x |& grep -q "Unknown toUnit x"  
	@./catPower 3.14 m j | grep -q "3.14 m is 1.31252E+16 j"  
	@./catPower 3.14 g e | grep -q "3.14 g is 2.37728E+27 e" 
	@./catPower 3.14 f m | grep -q "3.14 f is 7.51196E+08 m" 
	@./catPower 3.14 c g | grep -q "3.14 c is 0 g" 
	@./catPower 3.14 x f |& grep -q "Unknown fromUnit x" 
	@./catPower 3.14 c | grep -q  "Usage:  catPower fromValue fromUnit toUnit"
	@echo "All tests pass"
