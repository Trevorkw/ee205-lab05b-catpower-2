/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file m.cpp
/// @version 1.0
///
/// @author Trevor Chang <trevorkw@hawaii.edu>
/// @date 13 Feb 2022
/////////////////////////////////////////////////////////////////////////////
//
#include "m.h"
double fromMegatonToJoule( double megaton ) {
   return megaton / MEGATON_IN_A_JOULE;
}
double fromJouleToMegaton( double joule ) {
   return joule * MEGATON_IN_A_JOULE;
}
